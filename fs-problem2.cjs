const fs = require('fs');
const path = require('path');

function fsProblem2(lipsum, cb) {
  // 1. Read the given file lipsum.txt
  fs.readFile(lipsum, 'utf-8', (err, data) => {
    if (err) {
      console.error('Error reading lipsum.txt:', err.message);
      return;
    } else {
      console.log('successfully reading content.');
    }

    console.log(data.toString());

    // 2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt.
    const upper_case_content = data.toUpperCase();

    fs.writeFile('upper_case.txt', upper_case_content, (err) => {
      if (err) {
        console.error('Error writing to upper_case.txt:', err.message);
        return;
      } else {
        console.log('Saved successfully.');
      }

      // Append filename to filenames.txt
      fs.appendFile('filenames.txt', 'upper_case.txt' + '\n', (err) => {
        if (err) {
          console.error('Error appending filename to filenames.txt:', err.message);
        } else {
          console.log('Appended successfully.');
        }

        // 3. Read the new file, convert it to lower case, split the contents into sentences, and write it to a new file. Store the name of the new file in filenames.txt.
        const lower_case_content = upper_case_content.toLowerCase();
        const lower_split_sentences = lower_case_content.split('.').join('\n');

        fs.writeFile('lower_sentence.txt', lower_split_sentences, (err) => {
          if (err) {
            console.error('Error writing to lower_sentence.txt:', err.message);
            return;
          } else {
            console.log('successfully Written file.');
          }

          // Append filename to filenames.txt
          fs.appendFile('filenames.txt', 'lower_sentence.txt' + '\n', (err) => {
            if (err) {
              console.error('Error appending filename to filenames.txt:', err.message);
            } else {
              console.log('Saved successfully.');
            }

            // 4. Read the new file, sort the content, write it out to a new file. Store the name of the new file in filenames.txt.
            const sorted_content = lower_split_sentences.split('\n').sort().join('\n');

            fs.writeFile('sorted.txt', sorted_content, (err) => {
              if (err) {
                console.error('Error writing to sorted.txt:', err.message);
                return;
              } else {
                console.log('Saved successfully.');
              }

              console.log('Successfully saved sorted content.');

              // Append filename to filenames.txt
              fs.appendFile('filenames.txt', 'sorted.txt', (err) => {
                if (err) {
                  console.error('Error appending filename to filenames.txt:', err.message);
                  return;
                } else {
                  console.log('Saved successfully.');
                }

                // 5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
                fs.readFile('filenames.txt', 'utf-8', (err, data) => {
                  if (err) {
                    console.error('Error reading filenames.txt:', err.message);
                    return;
                  } else {
                    console.log('Saved successfully.');
                  }

                  const data_array = data.split('\n');

                  for (let index = 0; index < data_array.length; index++) {
                    fs.unlink(data_array[index], (err) => {
                      if (err) {
                        console.error('Error deleting file:', err.message);
                      } else {
                        console.log('Deleted without any error:', data_array[index]);
                      }
                    });
                  }
                });
              });
            });
          });
        });
      });
    });
  });
};

module.exports = fsProblem2;