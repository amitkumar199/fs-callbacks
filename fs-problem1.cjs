const fs = require('fs');
const path = require('path');

function fsProblem1(absolutePathOfRandomDirectory, randomNumberOfFiles){
    const files_array=[];

    fs.mkdir(absolutePathOfRandomDirectory,(err)=>{
        if(err){
            console.error("Can not perform operation:", err.message);
        }else{
            console.log("dirctory created");
            
            for(let index=0; index<randomNumberOfFiles; index++){
                const jsonData = {
                    id: index,
                    data: `Random data for file ${index}`
                };
                const json_file = `created${index}.json`;

                fs.writeFile(path.join(absolutePathOfRandomDirectory, json_file), JSON.stringify(jsonData), (err)=>{
                    if(err){
                        console.error("Can not perform operation:", err.message);
                    }else{
                        console.log("files saved", json_file);
                        files_array.push(json_file);
                        
                        if(files_array.length===randomNumberOfFiles){
                            remove_files(absolutePathOfRandomDirectory, files_array);
                        }
                    }
                });
            }
        }
    });
};

function remove_files(absolutePathOfRandomDirectory, files_array){
    for(let index=0; index<files_array.length; index++){
        const file_path= path.join(absolutePathOfRandomDirectory, files_array[index]);

        fs.unlink(file_path, (error)=>{
            if(error){
                console.error("can not perform", error.message);
            }else{
                console.log("files removed:", files_array[index]);
            } 
       });
    }
};

module.exports = fsProblem1;
